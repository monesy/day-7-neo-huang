package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("employees")
class EmployeesController {
    private EmployeeRepository employeeRepository;

    public EmployeesController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping
    public List<Employee> getEmployeeList() {
        return new ArrayList<>(employeeRepository.getEmployees().values());
    }

    @GetMapping("/{employeeId}")
    public Employee getEmployeeById(@PathVariable("employeeId") int employeeId) {
        return employeeRepository.getEmployees().get(employeeId);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(@RequestParam("gender") String gender) {
        return employeeRepository.getEmployees().values()
                .stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    @PostMapping
    public void createEmployee(@RequestBody Employee employee) {
        employee.setId(nextId());
        employeeRepository.getEmployees().put(nextId(), employee);
    }

    @PutMapping("/{employeeId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void updateEmployeeById(@PathVariable("employeeId") int employeeId,
                                   @RequestParam("salary") double salary, @RequestParam("age") int age) {

        Optional.ofNullable(employeeRepository.getEmployees().get(employeeId))
                .ifPresent(employee -> {
                    updateEmployeeWithAgeAndSalary(employeeId, salary, age, employee);
                });
    }

    private void updateEmployeeWithAgeAndSalary(int employeeId, double salary, int age, Employee employee) {
        employee.setAge(age);
        employee.setSalary(salary);
        employeeRepository.getEmployees().put(employeeId, employee);
    }

    @DeleteMapping("/{employeeId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void deleteEmployeeById(@PathVariable("employeeId") int employeeId) {
        employeeRepository.getEmployees().remove(employeeId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeesPageQuery(@RequestParam("page") int page, @RequestParam("size") int size) {
        List<Employee> pageSizeEmployees = employeeRepository.getEmployees()
                .values()
                .stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
        if (pageSizeEmployees.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        return pageSizeEmployees;
    }

    private Integer nextId() {
        int maxId = employeeRepository.getEmployees()
                .values()
                .stream()
                .mapToInt(Employee::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }


}