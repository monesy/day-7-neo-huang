package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CompanyRepository {
    private Map<Integer, Company> companies = new HashMap();

    public CompanyRepository() {
        for (int i = 0; i < 20; i++) {
            companies.put(i, new Company(i, "Company " + i));
        }
    }

    public Map<Integer, Company> getCompanies() {
        return companies;
    }
}
