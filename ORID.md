### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	上午进行了Code Review，Lucy指出我的git message不正确，然后进行了关于“What is refactoring”的Concept Map的绘制，我作为演讲人员进行了Concept Map的介绍，然后学习了HTTP Basic和RESTful的简单学习，为下午的课程做铺垫。下午学习了Spring Boot的的内容，我们简单了解了基本知识就开始上手写代码，写了关于公司员工管理的案例。

### R (Reflective): Please use one word to express your feelings about today's class.

​	充实

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	非常非常感谢王强同学，没有他给我提供的帮助与鼓励，我不可能如此快速且舒适的学会Spring Boot的基础用法。在下午的Spring Boot课程中，一开始老师讲的很模糊抽象，在后面上手写代码并在Pair Programming中得益于王强同学毫无保留且充满耐心的教导，我才理解了Spring Boot在web开发中的基本操作。今天的经历让我知道了实践对于掌握知识的帮助是无与伦比的。

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	接下来认真学习Spring Boot的知识，并多多动手练习，熟能生巧。
